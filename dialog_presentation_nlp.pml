<?xml version="1.0" encoding="UTF-8" ?>
<Package name="dialog_presentation_nlp" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions/>
    <Dialogs>
        <Dialog name="dlg_presentation_nlp" src="dlg_presentation_nlp/dlg_presentation_nlp.dlg" />
    </Dialogs>
    <Resources>
        <File name="style" src="html/css/style.css" />
        <File name="index" src="html/index.html" />
        <File name="icon" src="icon.png" />
        <File name="icon" src="html/cover.jpg" />
        <File name="icon" src="html/inf-lg-text-cze-rgb.png" />
        <File name="icon" src="html/logo_NLPlab.png" />
        <File name="icon" src="html/ptakopysk.png" />
        <File name="icon" src="html/tree.png" />
        <File name="icon" src="html/corpora.png" />
        <File name="icon" src="html/krabice.jpg" />
        <File name="icon" src="html/vl_anim.gif" />
        <File name="jquery-1.11.0.min" src="html/js/jquery-1.11.0.min.js" />
        <File name="main" src="html/js/main.js" />
        <File name="robotutils" src="html/js/robotutils.js" />
    </Resources>
    <Topics>
        <Topic name="dlg_presentation_nlp_czc" src="dlg_presentation_nlp/dlg_presentation_nlp_czc.top" topicName="dlg_presentation_nlp" language="cs_CZ" />
        <Topic name="dlg_presentation_nlp_enu" src="dlg_presentation_nlp/dlg_presentation_nlp_enu.top" topicName="dlg_presentation_nlp" language="en_US" />
    </Topics>
    <IgnoredPaths>
        <Path src=".metadata" />
    </IgnoredPaths>
</Package>
