# Dialog of NLP lab presentation

Developed at [NLP Centre](https://nlp.fi.muni.cz/en), [FI MU](https://www.fi.muni.cz/index.html.en) for [Karel Pepper](https://nlp.fi.muni.cz/trac/pepper)

This is a dialog application, in both Czech and English, that shows a presentation about NLP lab with images on the tablet.

Start with "Show presentation." / "Ukaž prezentaci."

## Installation

* [make and install](https://nlp.fi.muni.cz/trac/pepper/wiki/InstallPkg) the package as usual for the Pepper robot


